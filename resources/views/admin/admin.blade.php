@extends('layouts.master')

@section('content')

<div class="container">
@include('common.errors')
@include('common.status')
	<h3>Administration <small class="pull-right">{{ $admin->username }}</small> </h3><br />
	<div class=row">
	<div class="panel panel-default">
		<div class="panel-heading">
			<div class="panel-title">
				Réglages généraux
			</div> 
		</div>
		<div class="panel-body">
			<b>Nom du site :</b> {{ $name }} 
			<form class="form-inline" action="{{ URL::route('mod_name') }}" method="post"> 
				{{ csrf_field() }}
				<input class="form-control input-sm" type="text" name="site_name">
				<button class="btn btn-sm btn-info">Modifier</button>
			</form>
			<hr>
			<b>Compte à rebours des paniers :</b> {{ $cd['hrs'] }} heure(s) {{ $cd['min'] }} minute(s)
			<br>
			<form class="form-inline" action="{{ URL::route('mod_cd') }}" method="post">
				{{ csrf_field() }}
				<input class="form-control input-sm" type="number" min="0" max="24" name="hours"> heures
				<input class="form-control input-sm" type="number" min="0" max="59" name="minutes"> minutes
				<button class="btn btn-sm btn-info">Modifier</button>
			</form>
			<br>
			<div class="alert alert-danger">Cette modification peut entrainer la suppression du paniers existants ! </div>
		</div>
	</div>
</div>


@stop
