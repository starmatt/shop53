<html>
<head>
  <title>Shop53</title>
  <script>window.Laravel = { csrfToken: '{{ csrf_token() }}' }</script>
  <meta name="_token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="/css/app.css">
</head>

<body>

  <div id="app">

    @include('layouts.navbar')

    <div class="s53__body">

      <s53-sidebar></s53-sidebar>

      <div class="container">
        <div class="col-xs-12 s53__main">

          <s53-items></s53-items>

        </div>
      </div>
    </div>

  </div>

  {{-- @include('layouts.footer') --}}

  <script src="/js/app.js"></script>
</body>

</html>