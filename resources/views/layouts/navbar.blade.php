<div class="s53__nav">
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
		<a class="s53__brand" href="/">Shop 53</a>
	</div>
	<div class="col-lg-offset-3 col-lg-5 col-md-offset-3 col-md-5 col-sm-offset-1 col-sm-7 col-xs-12">
		<div class="s53__searchbar s53__util-relative">
			<input class="s53__searchbar-input" type="text" placeholder="Search">
			<button class="s53__searchbar-btn" type="submit"><span class="icon-magnifier"></button>
		</div>
	</div>
</div>