<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_item', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('order_id')->unsigned();
			$table->integer('item_id')->unsigned();
			$table->string('item_name');
			$table->integer('amount')->unsigned();
			$table->float('price', 10, 2)->unsigned();
			$table->float('total', 10, 2)->unsigned();
		});

		Schema::table('order_item', function (Blueprint $table) {
			// Order relationship
			$table->foreign('order_id')
				->references('id')
				->on('orders');
			// Item relationship
			$table->foreign('item_id')
				->references('id')
				->on('items');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_item');
	}
}
