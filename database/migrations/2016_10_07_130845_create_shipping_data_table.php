<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_data', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('order_id')->unsigned();
			$table->string('firstname');
			$table->string('lastname');
			$table->string('street');
			$table->integer('post_code');
			$table->string('city');
			$table->string('country');
            $table->timestamps();
        });

		Schema::table('shipping_data', function (Blueprint $table) {
			$table->foreign('order_id')
				->references('id')
				->on('orders')
				->onUpdate('cascade')
				->onDelete('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shipping_data');
    }
}
