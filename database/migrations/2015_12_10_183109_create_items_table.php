<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('items', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->text('description')->nullable();
			$table->float('price', 10, 2)->unsigned();
			$table->integer('stock')->unsigned();
			$table->string('img_url');
			$table->integer('category_id')->unsigned()->nullable();
			$table->timestamps();
		});

		Schema::table('items', function (Blueprint $table) {
			$table->foreign('category_id')
				->references('id')
				->on('categories')
				->onUpdate('cascade')
				->onDelete('set null');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('items');
	}
}
