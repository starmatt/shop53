<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('users')->insert([
			['email' => 'exemple@mail.com',
			'password' => Hash::make('password'),
			'level' => 0,
			'firstname' => 'John',
			'lastname' => 'Doe'],

			['email' => 'bigboss@admin.com',
			'password' => Hash::make('admin'),
			'level' => 1,
			'firstname' => 'Boss',
			'lastname' => 'Big'],

			['email' => 'exemple1@mail.com',
			'password' => Hash::make('password'),
			'level' => 0,
			'firstname' => 'John',
			'lastname' => 'Doe'],

			['email' => 'exemple2@mail.com',
			'password' => Hash::make('password'),
			'level' => 0,
			'firstname' => 'John',
			'lastname' => 'Doe'],

			['email' => 'exemple3@mail.com',
			'password' => Hash::make('password'),
			'level' => 0,
			'firstname' => 'John',
			'lastname' => 'Doe'],

			['email' => 'exemple4@mail.com',
			'password' => Hash::make('password'),
			'level' => 0,
			'firstname' => 'John',
			'lastname' => 'Doe']
			]);
	}
}
