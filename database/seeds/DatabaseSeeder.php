<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
	public function run()
	{
		$this->call(UsersTableSeeder::class);
		$this->call(MasterTableSeeder::class);
		$this->call(CategoriesTableSeeder::class);
		$this->call(ItemsTableSeeder::class);
	}
}
