<?php

use Illuminate\Database\Seeder;

class MasterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('master')->insert(['site_name' => 'Shop53', 'cart_timeout' => 600]);
    }
}
