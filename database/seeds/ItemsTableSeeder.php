<?php

use App\Models\Item;
use Illuminate\Database\Seeder;

Class ItemsTableSeeder extends Seeder
{
	public function run()
	{
		Item::create([
			'name' => 'Truc',
			'description' => 'non applicable',
			'price' => '99.99',
			'stock' => 5,
			'img_url' => '/images/items/filler.jpg',
			'category_id' => 1
			]);

		Item::create([
			'name' => 'Machin',
			'description' => 'non applicable',
			'price' => '23.99',
			'stock' => 3,
			'img_url' => '/images/items/filler.jpg',
			'category_id' => 2
			]);

		Item::create([
			'name' => 'Bidule',
			'description' => 'non applicable',
			'price' => '123.99',
			'stock' => 0,
			'img_url' => '/images/items/filler.jpg',
			'category_id' => 3
			]);
		
		Item::create([
			'name' => 'Truc #2',
			'description' => 'non applicable',
			'price' => '99.99',
			'stock' => 5,
			'img_url' => '/images/items/filler.jpg',
			'category_id' => 1
			]);

		Item::create([
			'name' => 'Machin #2',
			'description' => 'non applicable',
			'price' => '23.99',
			'stock' => 3,
			'img_url' => '/images/items/filler.jpg',
			'category_id' => 2
			]);

		Item::create([
			'name' => 'Bidule #2',
			'description' => 'non applicable',
			'price' => '123.99',
			'stock' => 0,
			'img_url' => '/images/items/filler.jpg',
			'category_id' => 3
			]);
	}
}
