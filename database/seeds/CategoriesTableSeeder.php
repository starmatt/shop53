<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

Class CategoriesTableSeeder extends Seeder
{
	public function run()
	{
		Category::create([
			'name' => 'Category 1'
		]);

		Category::create([
			'name' => 'Category 2'
		]);

		Category::create([
			'name' => 'Category 3'
		]);
	}
}
