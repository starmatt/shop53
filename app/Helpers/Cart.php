<?php

namespace App\Helpers;

use Auth;
use Illuminate\Support\Collection;

class Cart
{
	public static function get()
	{
		return session('cart_' . self::name());
	}

	public static function getGuest()
	{
		return session('cart_guest');
	}

	public static function getAll()
	{
		$array = session()->all();
		$pattern = '/^cart_/';

		return array_intersect_key($array, array_flip(preg_grep($pattern, array_keys($array))));
	}

	public static function make()
	{
		return collect([

			'total' => 0,
			'timestamp' => null
			]);
	}

	public static function save(Collection $cart)
	{
		$cart['items']->isEmpty() ?
			session()->forget('cart_' . self::name()) : session(['cart_' . self::name() => $cart]);
	}

	public static function saveMany($carts)
	{
		foreach ($carts as $name => $cart) {
			if (!$cart['items']->isEmpty()) {
				session([$name => $cart]);
			}
		}
	}

	public static function merge($first, $second)
	{
		foreach ($second['items'] as $item) {
			$first['items'] = $item->mergeItem($first['items']);
		}

		return collect([
			'items' => $first['items'],
			'total' => $first['total'] + $second['total'],
			'timestamp' => $first['timestamp']->gt($second['timestamp']) ?
				$first['timestamp'] : $second['timestamp'],
		]);
	}

	public static function destroy($name = null)
	{
		$name !== null ? session()->forget($name) : session()->forget('cart_' . self::name());
	}

	public static function name()
	{
		return !Auth::check() ? 'guest' : Auth::user()->id;
	}

	public static function check(string $name)
	{
		return session()->has('cart_' . $name);
	}
}
