<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class UserController extends Controller
{
	private $user;

	public function __construct()
	{
		$this->user = Auth::user();
	}

	public function index()
	{
		return view('user.profile', [
			'user' => $this->user,			
			]);
	}
}