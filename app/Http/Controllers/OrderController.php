<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Helpers\Cart;
use Validator;
use Auth;

use App\Models\Order;
use App\Models\ShippingData;


class OrderController extends Controller
{
	/*
	 * Order statuses:
	 * 			- 0: Cancelled
	 * 			- 1: Shipped,
	 * 			- 2: Payment accepted,
	 * 			- 3: Unpaid
	 */

	public function make(Request $request)
	{
		$rules = [
			'address' => 'required',
			'street' => 'required_if:address,new',
			'postcode' => 'required_if:address,new|numeric',
			'city' => 'required_if:address,new',
			'country' => 'required_if:address,new',
			];

		if (!Auth::check()) {
			$rules = array_merge($rules, [
				'firstname' => 'required',
				'lastname' =>'required',
				]);
		}

		$v = Validator::make($request->all(), $rules);

		if ($v->fails()) {
			return redirect()->back()->withErrors($v, 'ship');
		}

		if (!($cart = Cart::get())) {
			return redirect()->back();
		}

		$order = new Order;
		$order->user_id = Auth::user() ? Auth::user()->id : null; 
		$order->total = $cart['total'];
		$order->status = '3';
		$order->save();

		foreach ($cart['items'] as $item) {
			$order->linkOrderItem($item);
		}

		if ($request->address === 'new') {
			$order->linkShippingData($request->all());
		}

		Cart::destroy();

		return redirect('/')->with('status', 'Order accepted');
	}
}
