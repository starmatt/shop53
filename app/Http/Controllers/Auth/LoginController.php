<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Helpers\Cart;
use Auth;

class LoginController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles authenticating users for the application and
	| redirecting them to your home screen. The controller uses a trait
	| to conveniently provide its functionality to your applications.
	|
	 */

	use AuthenticatesUsers;

	/**
	 * Where to redirect users after login.
	 *
	 * @var string
	 */
	protected $redirectTo = '/';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest', ['except' => 'logout']);
	}

	public function logout(Request $request)
	{
		$carts = Cart::getAll();

		$this->guard()->logout();
		$request->session()->flush();
		$request->session()->regenerate();

		Cart::saveMany($carts);

		return redirect('/');
	}

	protected function sendLoginResponse(Request $request)
	{
		if (Cart::check('guest') && !Cart::check(Auth::user()->id)) {
			Cart::save(Cart::getGuest());
			Cart::destroy('cart_guest');
		} elseif (Cart::check('guest') && Cart::check(Auth::user()->id)) {
			Cart::save(
				Cart::merge(Cart::getGuest(),
				Cart::get())
			);
			Cart::destroy('cart_guest');

			$request->session()->flash('status', 'Notification: new items were added to your cart!');
		}

		$request->session()->regenerate();

		$this->clearLoginAttempts($request);

		return $this->authenticated($request, $this->guard()->user())
			?: redirect()->intended($this->redirectPath());
	}
}
