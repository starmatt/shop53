<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Models\Item;

class IndexController extends Controller
{
	public function index()
	{
		return view('front.home');
	}

	public function fetchItems(Request $request)
	{
		$items = Item::latest()
			->where('stock', '>', 0)
			->limit(4)
			->orderBy('created_at', 'desc')
			->get();

		return $items;
	}
}
