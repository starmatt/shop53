<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;

class ApiController extends Controller
{
	public function isConnected()
	{
		return Auth::check();
	}
}
