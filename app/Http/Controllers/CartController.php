<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Helpers\Cart;
use Auth;
use Validator;
use DB;
use Carbon\Carbon;

use App\Models\{
	Item
};

class CartController extends Controller
{
	public function index()
	{
		if (!($cart = Cart::get())) {
			return redirect('/')->withErrors('Empty cart');
		}

		$timeout = DB::table('master')->first()->cart_timeout;
		//$cooldown = Carbon::now()->subMinutes($timeout / 60)->diffInMinutes($cart['timestamp']);
		$cooldown = Carbon::now()->subSeconds($timeout)->diffInSeconds($cart['timestamp']) / 60;

		return view('cart', [
			'items' => $cart['items'],
			'total' => $cart['total'],
			'cooldown' => $cooldown,
			]);
	}

	public function add(Request $request)
	{
		$v = Validator::make($request->all(), [
			'item' => 'required|exists:items,id'
			]);

		if ($v->fails()) {
			return redirect()->back()->withErrors($v, 'item_add');
		}

		$item = Item::find($request->item);

		if (!$item->stock() || !$item->addToCart()) {
			return redirect()->back()->withErrors(['item_stock' => 'Item is out of stock']);
		}

		return redirect()->back();
	}

	public function del(Item $item)
	{
		if (!$item->removeOneFromCart()) {
			return redirect()->back()->withErrors(['item_del' => 'Illegal action']);
		}

		return redirect()->back();
	}

	public function destroy(Request $request)
	{
		Cart::destroy();
		return redirect('/')->with('success', 'Cart emptied');
	}
}
