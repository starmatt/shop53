<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Helpers\Cart;
use Session;

class AppServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		view()->composer(['layouts.master'], function ($v) {
			$c = 0;
			if (($cart = Cart::get()) !== null) {
				foreach ($cart['items'] as $item) {
					$c += $item->amount;
				}
			}
			$v->with('items_count', $c);
		});
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}
}
