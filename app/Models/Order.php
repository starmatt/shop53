<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Item;

class Order extends Model
{
	protected $fillable = [
		'user_id',
		'total',
		'status'
		];

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function orderItems()
	{
		return $this->belongsToMany(Item::class, 'order_item')
			->withPivot('item_name', 'amount', 'price', 'total');
	}

	public function shippingData()
	{
		return $this->hasOne(ShippingData::class);
	}

	public function linkOrderItem(Item $item) 
	{
		$this->orderItems()->attach($item, [
			'item_name' => $item->name,
			'amount' => $item->amount,
			'price' => $item->price,
			'total' => $item->price * $item->amount,
			]);
	}

	public function linkShippingData($data)
	{
		return $this->shippingData()->create([
			'firstname' => $data['firstname'],
			'lastname' => $data['lastname'],
			'street' => $data['street'],
			'post_code' => $data['postcode'],
			'city' => $data['city'],
			'country' => $data['country'],
			]);
	}
}
