<?php

namespace App\Models;

use Cookie;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
	use Notifiable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'firstname',
		'lastname',
		'level',
		'email',
		'password'
		];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
		];

	public function addresses()
	{
		return $this->hasMany('App\Model\UserAddress');
	}

	public function defaultAddress()
	{
		return $this->addresses->whereStrict('default', true);
	}
}