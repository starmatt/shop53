<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use App\Helpers\Cart;

class Item extends Model
{
	protected $fillable = [
		'name',
		'description',
		'price',
		'stock',
		'img_url',
		'category_id'
		];

	public function category()
	{
		return $this->belongsTo(Category::class);
	}

	public function addToCart()
	{
		$cart = Cart::get() ?: Cart::make();

		if ($cart['items']->has($this->id) &&
			$cart['items'][$this->id]->amount >= $this->stock) {
 			return false;
		}

		$cart['items'] = $this->mergeItem($cart['items']);
		$cart['total'] += $this->price;
		$cart['timestamp'] = Carbon::now();

		Cart::save($cart);

		return true;
	}
	
	public function mergeItem(Collection $items)
	{
		if ($items->has($this->id)) {
			$amount = $this->amount ?: 1;
			$items[$this->id]->amount += $amount;
		} else {
			$this->amount = 1;
			$items[$this->id] = $this;
		}

		return $items;
	}

	public function removeOneFromCart()
	{
		if(!($cart = Cart::get())) {
			return false;
		}

		if ($cart['items'][$this->id]->amount === 1) {
			$cart['items']->forget($this->id);
		} else {
			$cart['items'][$this->id]->amount -= 1;
		}

		$cart['total'] -= $this->price;
		Cart::save($cart);

		return true;
	}

	public function stock()
	{
		return $this->stock >= 1;
	}
}
