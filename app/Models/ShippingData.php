<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShippingData extends Model
{
	protected $table = 'shipping_data';

	protected $fillable = [
		'order_id',
		'firstname',
		'lastname',
		'street',
		'post_code',
		'city',
		'country'
		];

	public function order()
	{
		return $this->belongsTo(App\Models\Order::class);
	}
}
