<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();

Route::get('/', 'IndexController@index');

// Cart
Route::get('/cart', 'CartController@index');
Route::post('/cart/add', 'CartController@add');
Route::post('/cart/del/{item}', 'CartController@del');
Route::post('/cart/destroy', 'CartController@destroy');

// Orders
Route::post('/order', 'OrderController@make');
Route::get('/user/orders', 'OrderController@getIndex');

// Users
Route::group(['middleware' => 'auth', 'prefix' => 'user'], function () {
	Route::get('/', 'UserController@index');
	Route::post('/settings/email', 'UserController@postEmail');
	Route::post('/settings/addr', 'UserController@postAddress');
	Route::get('/settings/addrLess', 'UserController@removeAddr');
	Route::post('/settings/name', 'UserController@postFullname');
	Route::get('/settings/nameLess', 'UserController@removeName');
	Route::get('/orders', 'OrderController@index');
});

// Administration
Route::get('/admin', 'Admin\AdminController@getIndex');
Route::post('/admin/mod_cd', [
	'as' => 'mod_cd',
	'uses' => 'Admin\AdminController@cdModify'
	]);
Route::post('/admin/mod_name', [
	'as' => 'mod_name',
	'uses' => 'Admin\AdminController@nameModify'
	]);
// Users
Route::post('/admin/users/mod/{id}', [
	'as' => 'mod_user',
	'uses' => 'Admin\AdminUserController@userModify'
	]);
// Items
Route::post('/admin/items/mod/{id}', [
	'as' => 'mod_item',
	'uses' => 'Admin\AdminItemController@itemModify'
	]);
Route::post('/admin/items/add', [
	'as' => 'add_new_item',
	'uses' => 'Admin\AdminItemController@itemAdd'
	]);
// Categories
Route::post('/admin/categories/mod/{id}', [
	'as' => 'mod_category',
	'uses' => 'Admin\AdminCategoryController@categoryModify'
	]);
Route::post('/admin/categories/add', [
	'as' => 'add_new_category',
	'uses' => 'Admin\AdminCategoryController@categoryAdd'
	]);
// Orders
Route::post ('admin/orders/mod/{id}', [
	'as' => 'mod_order',
	'uses' => 'Admin\AdminOrderContoller@orderModify'
	]);
